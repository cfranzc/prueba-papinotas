import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar';
import Face from '@material-ui/icons/Face';
import { deepOrange } from '@material-ui/core/colors';
import CssBaseline from '@material-ui/core/CssBaseline';

import Personaje from '../Home/Personaje';

import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { Grid } from '@material-ui/core';

const GET_LIST = gql`
	# Ya que no sabemos qué buscaran nuestros usuarios, tenemos que hacer
	# una petición con variables; para cada vez que hacemos una búsqueda 
	# cambiar la petición y obtener el resultado correcto.
    
    query ($searchText: String) {
        allPersons (
          filter : { name_contains : $searchText}
        ){
          id
          name
          homeworld{
            name
          }
          films{
            title
          }
          starships{
            name
          }
        }
      }
`;

const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
        marginBottom: 10,
        marginTop: 10,
        display: 'flex',
    },
    content: {
        flexGrow: 1,
    },
    orangeAvatar: {
        color: '#fff',
        backgroundColor: deepOrange[500]
    },
    card: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 1,
    },
    iconButton: {
        padding: 10,
    }
});


class Lista extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            selected: null,
            dataSelected: null,
        }
        this.handleClickOpen = this.handleClickOpen.bind(this)
        this.handleClose = this.handleClose.bind(this)
    }

    handleClickOpen(data) {
        this.setState({
            open: true,
            selected: data.id,
            dataSelected: data,
        })
    }

    handleClose() {
        this.setState({
            open: false,
            selected: null,
            dataSelected: null,
        })
    }

    render() {
        const { classes, searchText } = this.props;
        const { open, selected, dataSelected } = this.state

        return (
            <React.Fragment>
                <CssBaseline />
                <Grid className={classes.root}>
                    <List component="nav" className={classes.content} aria-label="mailbox folders">
                        <Query query={GET_LIST} variables={{ searchText }}>
                            {({ loading, error, data }) => {
                                if (loading) return <p>Cargando...</p>;
                                if (error) return <p>Lo sentimos, ha ocurrido un error...</p>;
                                if (data) {
                                    return (
                                        data.allPersons.map((person) => (
                                            <ListItem button divider key={person.id} onClick={() => { this.handleClickOpen(person) }} >
                                                <ListItemAvatar>
                                                    <Avatar className={classes.orangeAvatar}>
                                                        <Face />
                                                    </Avatar>
                                                </ListItemAvatar>
                                                <ListItemText primary={person.name} secondary={(!person.homeworld) ? "Sin origen" : person.homeworld.name} />

                                            </ListItem>

                                        ))
                                    )
                                }
                            }}
                        </Query>
                    </List>
                    {
                        open && <Personaje
                                    open={open}
                                    selected={selected}
                                    handleClose={this.handleClose}
                                    dataSelected={dataSelected}
                                />
                    }
                </Grid>
            </React.Fragment>
        );
    }
}
Lista.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Lista);