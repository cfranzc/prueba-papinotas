import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Bookmark from '@material-ui/icons/Bookmark';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import StarBorder from '@material-ui/icons/StarBorder';
import Star from '@material-ui/icons/Star';
import IconButton from '@material-ui/core/IconButton';

import LocalData from '../../config/LocalData';

import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const GET_INFO = gql`
	# Ya que no sabemos qué buscaran nuestros usuarios, tenemos que hacer
	# una petición con variables; para cada vez que hacemos una búsqueda 
	# cambiar la petición y obtener el resultado correcto.
    
    query ($selected: ID!) {
        allPersons (
          filter : { id : $selected}
        ){
          id
          name
          homeworld{
            name
          }
          films{
            title
          }
          starships{
            name
          }
          species{
            name,
            classification,
            designation,
            language
          }
        }
      }
`;


const styles = theme => ({
    card: {
        minWidth: 275,
    },
    pos: {
        marginBottom: 1,
    },
    iconButton: {
        padding: 10,
    }
});


class Personaje extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            favorite: LocalData.existFavorite(this.props.selected),
        }
    }

    async addFavorite() {
        if(this.state.favorite){
            await LocalData.clearFavorites(this.props.selected)
            this.setState({
                favorite:false
            })
        }else{
            var person = await this.props.dataSelected
            await LocalData.setFavorites(person)
            this.setState({
                favorite:true
            })
        }
        
    }

    render() {
        const { classes, selected, open } = this.props
        const { favorite } = this.state
        return (
            <React.Fragment>
                <Dialog
                    open={open}
                    keepMounted
                    onClose={this.props.handleClose}
                    maxWidth='md'
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle id="alert-dialog-slide-title"><Bookmark style={{ fontSize: 15}} /> Información de Personaje</DialogTitle>
                    <DialogContent dividers>
                        {
                            (selected != null) ? (
                                <Query query={GET_INFO} variables={{ selected }}>
                                    {({ loading, error, data }) => {
                                        if (loading) return <p>Cargando...</p>;
                                        if (error) return <p>Lo sentimos, ha ocurrido un error...</p>;
                                        if (data) return (
                                            data.allPersons.map(({ id, name, homeworld, species, starships, films }) => (
                                                <Card className={classes.card} key={id}>
                                                    <CardContent>

                                                        <Typography className={classes.pos} color="textSecondary">
                                                            Nombre
                                                                </Typography>
                                                        <Typography variant="body2" component="p">
                                                            {name}
                                                        </Typography>

                                                        <Typography className={classes.pos} color="textSecondary">
                                                            Planeta de Origen
                                                                </Typography>
                                                        <Typography variant="body2" component="p">
                                                            {(!homeworld) ? ("Sin información") : (homeworld.name)}
                                                        </Typography>

                                                        <Typography className={classes.pos} color="textSecondary">
                                                            Especie
                                                        </Typography>
                                                        <Typography variant="body2" component="p">
                                                            {(species.length > 0) ? (species[0].name) : ("Sin información")}
                                                        </Typography>

                                                        <Typography className={classes.pos} color="textSecondary">
                                                            Naves
                                                        </Typography>

                                                        {(starships.length > 0) ? (
                                                            starships.map((starship, index) => (
                                                                <Typography key={index} variant="body2" component="p">
                                                                    - {starship.name}
                                                                </Typography>
                                                            ))
                                                        ) : (<Typography variant="body2" component="p">
                                                            Sin información
                                                        </Typography>)}


                                                        <Typography className={classes.pos} color="textSecondary">
                                                            Películas
                                                        </Typography>
                                                        {(films.length > 0) ? (
                                                            films.map((film, index) => (
                                                                <Typography key={index} variant="body2" component="p">
                                                                    - {film.title}
                                                                </Typography>
                                                            ))
                                                        ) : (<Typography variant="body2" component="p">
                                                            Sin información
                                                        </Typography>)}

                                                    </CardContent>
                                                    <CardActions>
                                                        <IconButton className={classes.iconButton} onClick={() => { this.addFavorite() }}>
                                                            { (favorite)?(
                                                                <Star color="primary" />
                                                            ):(
                                                                <StarBorder/>
                                                            ) }
                                                        </IconButton>
                                                    </CardActions>
                                                </Card>
                                            ))
                                        )
                                    }}
                                </Query>
                            ) : (
                                    <p>Sin registros</p>
                                )
                        }

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.handleClose} color="primary">
                            Volver
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        );
    }
}
Personaje.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Personaje);