import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import Star from '@material-ui/icons/StarBorder';

import Favoritos from '../Home/Favoritos'

const styles = theme => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: '100%',
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 4,
    },
});

class Busqueda extends React.Component {

    constructor(props){
        super(props)
    }
    render() {
        const {classes,searchText} = this.props
        return (
            <Paper className={classes.root}>
                <InputBase
                    className={classes.input}
                    value={searchText}
                    onChange={e => this.props.handleSearchChange(e)}
                    placeholder="Buscar personaje"
                    inputProps={{ 'aria-label': 'Buscar personaje' }}
                />
                <IconButton className={classes.iconButton} aria-label="busqueda">
                    <SearchIcon />
                </IconButton>
                <Divider className={classes.divider} orientation="vertical" />
                <Favoritos />
            </Paper>
        );
    }
}

Busqueda.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Busqueda);
