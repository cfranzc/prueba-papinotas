import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { deepOrange } from '@material-ui/core/colors';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

import Star from '@material-ui/icons/Star';
import Delete from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

import LocalData from '../../config/LocalData';
import Personaje from '../Home/Personaje';

const styles = theme => ({
  list: {
    width: 300,
  },
  iconButton: {
    padding: 10,
  },
  title: {
    padding: 10
  },
  orangeAvatar: {
    color: deepOrange[500],
  },
});

class Favoritos extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      right: false,
      data: {},
      open: false,
      selected: null,
      dataSelected: null
    }
    this.toggleDrawer = this.toggleDrawer.bind(this)
    this.sideList = this.sideList.bind(this)
    this.handleClickOpen = this.handleClickOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  handleClickOpen(data) {
    this.setState({
      open: true,
      selected: data.id,
      dataSelected: data,
    })
  }

  handleClose() {
    this.setState({
      open: false,
      selected: null,
      dataSelected: null,
    })
  }

  toggleDrawer = (side, open) => event => {
    if (open === true) {
      this.setState({
        data: LocalData.getFavorites(),
      }, () => {
        //console.log(this.state.data)
      })
    } else {
      this.setState({ data: {} })
    }
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    this.setState({
      right: open
    });
  };

  deleteFavorite = id => {
    LocalData.clearFavorites(id)
  }

  sideList = side => {
    const { classes } = this.props
    const { data } = this.state
    return (
      <div
        className={classes.list}
        role="presentation"
        onClick={this.toggleDrawer(side, false)}
        onKeyDown={this.toggleDrawer(side, false)}
      >
        <Typography className={classes.title} variant="body2" component="p">
          Favoritos
      </Typography>
        <Divider />
        <List>
          {
            (Object.keys(data).length > 0) ? (
              Object.entries(data).map((data) => (

                <ListItem key={data[1].id} divider role={undefined} dense button onClick={() => { this.handleClickOpen(data[1]) }}>
                  <ListItemIcon>
                    <Star color="primary" />
                  </ListItemIcon>
                  <ListItemText id={data[1].id} primary={data[1].name} secondary={data[1].homeworld.name} />
                  <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="comments" onClick={() => { this.deleteFavorite(data[1].id) }}>
                      <Delete />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>

              ))
            ) : (
                <Typography className={classes.title} variant="body2" component="p" color="textSecondary">
                  Sin favoritos
            </Typography>
              )
          }
        </List>
      </div>
    )
  }

  render() {
    const { classes } = this.props;
    const { open, selected, dataSelected } = this.state

    return (
      <div>
        <IconButton onClick={this.toggleDrawer('right', true)} className={classes.iconButton} aria-label="directions">
          <Star />
        </IconButton>
        <Drawer anchor="right" open={this.state.right} onClose={this.toggleDrawer('right', false)}>
          {this.sideList('right')}
        </Drawer>
        {
          open && <Personaje
            open={open}
            selected={selected}
            handleClose={this.handleClose}
            dataSelected={dataSelected}
          />
        }
      </div>
    );
  }

}

Favoritos.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Favoritos);
