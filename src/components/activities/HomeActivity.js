import React from 'react';
import PropTypes from 'prop-types';

import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import logo from '../../assets/images/star-wars-logo-png-8.png'
import bgImge from '../../assets/images/bg-sw-2.jpg'

import Busqueda from '../Home/Busqueda'
import Lista from '../Home/Lista'

const styles = theme => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: `url(${bgImge})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        width: 200,
        height: 100,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
});

class HomeActivity extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null,
            searchText: 'Sky',
        }
        this.handleSearchChange = this.handleSearchChange.bind(this)
    }
    handleSearchChange(event) { 
        this.setState({
           searchText: event.target.value
        })
    }
    copyright() {
        return (
            <Typography variant="body2" color="textSecondary" align="center">
                
                <Link color="inherit" target="_blank" href="https://www.linkedin.com/in/carlos-franz-carvallo-fredes-938586142/">
                    Carlos Franz
                </Link>{' © '}
                {new Date().getFullYear()}
                {'. Hecho con '}
                <Link color="inherit" target="_blank" href="https://material-ui.com/">
                    Material-UI.
                </Link>
            </Typography>
        )
    }

    render() {
        const { classes } = this.props;
        const { searchText } = this.state
        return (
            <Grid container component="main" className={classes.root}>
                <CssBaseline />
                <Grid item xs={false} sm={4} md={4} className={classes.image} />
                <Grid item xs={12} sm={8} md={8} component={Paper} elevation={6} square>
                    <div className={classes.paper}>
                        <img src={logo} className={classes.avatar}></img>
                        <Typography component="h1" variant="h5">
                            Personajes
                        </Typography>
                        <form className={classes.form} noValidate>
                            <Busqueda
                                searchText={searchText}
                                handleSearchChange={this.handleSearchChange}
                            />
                            <Lista searchText={searchText} />
                            <Grid container>
                                <Grid item xs>
                                    <Link href="https://graphiql.graphcms.com/simple/v1/swapi" target="_blank" variant="body2">
                                        SWAPI
                                    </Link>
                                </Grid>
                                <Grid item>
                                    <Link href="https://graphql.org/learn/" target="_blank" variant="body2">
                                        GraphQL
                                    </Link>
                                </Grid>
                            </Grid>
                            <Box mt={5}>
                                {this.copyright()}
                            </Box>
                        </form>
                    </div>
                </Grid>
            </Grid>
        );
    }

}
HomeActivity.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HomeActivity);