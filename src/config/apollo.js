import ApolloClient from "apollo-boost";

export const client = new ApolloClient({
  uri: 'https://api.graphcms.com/simple/v1/swapi' // por defecto la URL seria '/graphql'
});