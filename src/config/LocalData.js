import { Component } from 'react';

export default class LocalData extends Component {

  static getFavorites()  {
    var data = JSON.parse(localStorage.getItem('favorites'));
    return data;
  };

  static async setFavorites(data) {
    var favorites = LocalData.getFavorites()
    favorites[data.id] = data
    localStorage.setItem('favorites', JSON.stringify(favorites))
  }

  static async clearFavorites(id){
    var favorites = await LocalData.getFavorites()
    if (id in favorites) {
      favorites[id] = undefined
      localStorage.setItem('favorites', JSON.stringify(favorites))    
    }
  }

  static existFavorite(id){
    var favorites = LocalData.getFavorites()
    if (id in favorites) {
      return true
    }else{
      return false
    }
  }
}
