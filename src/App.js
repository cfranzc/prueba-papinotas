import React, { Component } from "react";
import Button from '@material-ui/core/Button';

import { ApolloProvider } from 'react-apollo';
import { client } from './config/apollo'; // la configuración de apollo que acabamos de crear

import HomeActivity from './components/activities/HomeActivity'

class App extends Component {
    render() {
        return (
            <ApolloProvider client={client}> {/* en "client" debe ir la configuración de apollo */}
                <div className="App">
                    <HomeActivity />
                </div>
            </ApolloProvider>
        );
    }
}

export default App;